Para la creacion de un lanscape primero comence con la creación de un noise para utilizar como un heightmap.

Asi que lo primero que hize fue crear una clase de c++ llamada perlinNoiseC, esto debido a la utilizacion de vectores y funciones necesarias que tomaria más tiempo de realizar con blueprint.
Para utilizar la información del noise utilize un canvas render target, utilizando el tamaño de la misma para generarla, en la ultima version tiene un tamaño de 20 x 20.
Utilizando un blueprint y creandole un objeto del tipo perlinNoiseC, pinto sobrel el canvas utilizando dos ciclos, uno para los pixeles en X y otro para los pixeles en Y.

![](https://bitbucket.org/serbolsd/animal-photos/raw/ed15a230493a5b79078fb538a6dcfa788d24e8f0/img/creatin%20Noise.PNG)

![](https://bitbucket.org/serbolsd/animal-photos/raw/ed15a230493a5b79078fb538a6dcfa788d24e8f0/img/DrawTexture.PNG)
 
 ![](https://bitbucket.org/serbolsd/animal-photos/raw/ed15a230493a5b79078fb538a6dcfa788d24e8f0/img/Noise.PNG)

Después genero un material utilizando el render targert con el noise.

![](https://bitbucket.org/serbolsd/animal-photos/raw/ed15a230493a5b79078fb538a6dcfa788d24e8f0/img/Material.PNG)

Despues, activando el plugin para landmass, utilize una custom brush de material, en esta la paso el material generado anteriormente, y utilizo esta brush en una layer del landscape. El resultado es este:

![](https://bitbucket.org/serbolsd/animal-photos/raw/ed15a230493a5b79078fb538a6dcfa788d24e8f0/img/Landscape1.PNG)

Para el follage genere un material para el landscape con dos blender layer, una para pasto y otro para grava, para combinarlos mediante peso, donde el pasto tiene un mayor peso y por lo tanto mayor presencia.

![](https://bitbucket.org/serbolsd/animal-photos/raw/ed15a230493a5b79078fb538a6dcfa788d24e8f0/img/LandscaoeMaterial.PNG)

Despues genere un landscape grass type, para la generación de 2 tipos de pasto y 2 tipos de arboles.

![](https://bitbucket.org/serbolsd/animal-photos/raw/ed15a230493a5b79078fb538a6dcfa788d24e8f0/img/LGT.PNG)

Y para el spawn, en el material del landscape, utilizando un landscapegrassoutput vinculado al anterior grass type, y utilizando un sampler para especificar que se generen en el layer blender del pasto.

![](https://bitbucket.org/serbolsd/animal-photos/raw/265ef6f15fa3e2f3115e8d4aeffee825801845ff/img/Grass.PNG)

Al final se muestra algo asi:

![](https://bitbucket.org/serbolsd/animal-photos/raw/ed15a230493a5b79078fb538a6dcfa788d24e8f0/img/Landscape2.PNG)


La utilización de esto es muy variado, en mi proyecto yo lo uitlizo como mapa, genero 3 animales a los cuales tomarles fotos, estos animales spawnean en un punto aleatorio sobre el landscape y cuando le tomas fotos a todos se termina el juego.

**DIFICULTADES**
No pude hacer que se actuarizada el landscape con al mismo tiempo que se genera la textura, solo se actualiza cuando de modifica y vuelve a contruir la brush utilizando la función de render (o esa fue mi conclución).
Intente: 
- Entrar desde otro nivel.
- Crear una funcion para llamar la función render, pero no me fue posible.
- Modificar la clase padre en c++ de la brush, pero al compilar omitia mis cambios.
- Cambiar los datos de la brush desde otro blueprint para ver si se actualizaba.
- Intentar crear otro landscape desde un blueprint y setearle las cosas ya con el nuevo noise, pero no encontre como agregar muchas de las cosas que queria al landscape, entre ellas el heightmap y el tamaño de este.