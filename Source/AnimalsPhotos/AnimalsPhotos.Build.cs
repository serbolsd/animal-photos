// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class AnimalsPhotos : ModuleRules
{
	public AnimalsPhotos(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
