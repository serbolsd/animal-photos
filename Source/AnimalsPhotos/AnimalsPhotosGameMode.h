// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AnimalsPhotosGameMode.generated.h"

UCLASS(minimalapi)
class AAnimalsPhotosGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAnimalsPhotosGameMode();
};



