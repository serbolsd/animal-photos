// Copyright Epic Games, Inc. All Rights Reserved.

#include "AnimalsPhotosGameMode.h"
#include "AnimalsPhotosHUD.h"
#include "AnimalsPhotosCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAnimalsPhotosGameMode::AAnimalsPhotosGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AAnimalsPhotosHUD::StaticClass();
}
