// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <vector>
#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "perlinNoiseC.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class ANIMALSPHOTOS_API UperlinNoiseC : public UObject
{
	GENERATED_BODY()
   UFUNCTION(BlueprintCallable, Category = "Noise")
   float pnoise(float x, float y, float z);
 UFUNCTION(BlueprintCallable, Category = "NoiseSeed")
   void setSeed(int seed);

 std::vector<int> p;
 float fade(float t);
 float lerp(float t, float a, float b);
 float grad(int hash, float x, float y, float z);
};
